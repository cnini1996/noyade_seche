#But de l'exercice:
#Création d'un programme permettant de savoir le jour de la semaine pour une date donnée dans le calendrier grégorien

#Fonction permettrant de retouner un chiffre en fonction du mois choisi dans la date
def Ajout_mois(nombre):
    if nombre==1 or nombre==10:
        Amois = 0
    if nombre==2 or nombre ==3 or nombre==11:
        Amois=3
    if nombre==4 or nombre==7:
        Amois=6
    if nombre==5:
        Amois =1
    if nombre==6:
        Amois=4
    if nombre==8:
        Amois=2
    if nombre==9 or nombre==12:
        Amois=5
    return Amois

#Fonction permettant de déterminer si l'année choisie est bissextile ou non
def Bissextile(annee):
    bissextile=False
    if annee%400==0:
        bissextile=True
    elif annee%100==0:
        bissextile=False
    elif annee%4==0:
        bissextile=True
    return bissextile

#Fonction permettant de retourner un chiffre en fonction du siècle choisi dans la date
def Siecle(annee):
    s=int(annee[0:2])
    siecle = 0
    if s==16:
        siecle=6
    if s==17:
        siecle=4
    if s==18:
        siecle=2
    if s==19:
        siecle=0
    if s==20:
        siecle=6
    if s==21:
        siecle=4
    return siecle

#Fonction permettrant de retourner le jour de la date choisie en fonction du reste de la division
def Vjour(reste):
    journee=""
    if reste==0:
        journee="Dimanche"
    if reste==1:
        journee="Lundi"
    if reste==2:
        journee="Mardi"
    if reste==3:
        journee="Mercredi"
    if reste==4:
        journee="Jeudi"
    if reste==5:
        journee="Vendredi"
    if reste==6:
        journee="Samedi"
    return journee

#Début du programme
#L'utilisateur entre une date pour que l'on détermine le jour
VDate = input("Veuillez donner une date (AAAA-MM-JJ) : ")
VDate.split("-")
annee, mois, jour = VDate.split("-")
#On vérifie que la date rentrée soit possible et bien à partir du 01/11/1582
if annee <= "1582" or jour >"31" or mois>"12":
    print("Année invalide.")
    VDate = input("Veuillez donner une date supérieur  (AAAA-MM-JJ) : ")
    VDate.split("-")
    jour, mois, annee = VDate.split("-")
    if mois < "11":
        print("Année invalide.")
        VDate = input("Veuillez donner une date supérieur  (AAAA-MM-JJ) : ")
        VDate.split("-")
        jour, mois, annee = VDate.split("-")

print("Vous avez choisi la date : "+jour+"-"+mois+"-"+annee)

#on déclare une variable qui va permettre de calculer le jour de la date
calcul = 0

#On récupère les deux derniers chiffres de l'année choisie par l'utilisateur
der_chiffre = int(annee[2:4])

#On cherche à avoir la partie entière de la division entière pour le mettre dans le calcul final
reste = der_chiffre//4

#On utilise la fonction "Ajout_mois" pour ajouter une valeur au calcul final
Mois = Ajout_mois(int(mois))

#On utilise la fonction "Ajout_mois" pour ajouter une valeur au calcul final
Vbissextile=Bissextile(int(annee))
bissextile=0
#On vérifie si l'année est bissextile ou non pour opter ou non 1 au calcul final
if Vbissextile==True and (int(mois)==1 or int(mois)==2):
    bissextile=-1

#On utilise la fonction "Siecle" pour ajouter une valeur au calcul final
VSiecle=Siecle(annee)

#On fait le calcul final grâce à toutes les valeurs que l'on a récupéré précédemment
calcul = calcul + der_chiffre + reste + int(jour) + Mois + bissextile + VSiecle

#On récupère le reste du calcul
resultat=calcul%7

#On utilise la fonction "Vjour" pour déterminer la journée de la date choisie
journee=Vjour(resultat)
print("Le jour de la date que vous avez choisi est le : "+ journee)